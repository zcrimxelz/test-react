import { combineReducers } from 'redux';

const initHistoryState = {
    history: []
}

export const LOG_HISTORY = (state = initHistoryState, action) => {
    switch (action.type) {
        case 'INSERT_LOG':
            return {
                ...state,
                history : [...state.history,action.payload]
            }
        default:
            return {
                ...state
            }
    }
}

const rootReducer = combineReducers({
    LOG_HISTORY
});

export default rootReducer;

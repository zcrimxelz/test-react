export const insertLog = (message) => {
    return (dispatch) => {
        return dispatch({
            type: 'INSERT_LOG',
            payload: message
        })
    }
}
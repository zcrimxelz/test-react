import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { insertLog } from '../redux/actions/index'
import axios from 'axios'
import _ from 'lodash'

const Index = () => {
    const dispatch = useDispatch()
    const subHistory = useSelector(state => state.LOG_HISTORY)
    const [taskName, setTaskName] = useState("")
    const [taskIndex, setTaskIndex] = useState(null)
    const [listTaskName, setListTaskName] = useState([])
    const [listHistory, setListHistory] = useState([])
    const [isHoverDelete, setIsHoverDelete] = useState(false)
    const [isTyping, setIsTyping] = useState(false)



    useEffect(() => {
        axios.get('https://my-json-server.typicode.com/typicode/demo/posts').then(resp => {
            let taskNameFromAPI = _.map(resp.data, (value) => value.title)
            setListTaskName(taskNameFromAPI)
        })
    }, [])

    const toggleHover = () => {
        setIsHoverDelete(!isHoverDelete)
    }

    const toggleTyping = () => {
        setIsTyping(!isTyping)
    }

    const actionTaskName = () => {
        if (!!taskName) {
            if (!!taskIndex || taskIndex === 0) {
                let newTaskName = [...listTaskName];
                dispatch(insertLog(`Update name ${newTaskName[taskIndex]} to ${taskName}`))
                newTaskName[taskIndex] = taskName;
                setListTaskName(newTaskName);
            } else {
                dispatch(insertLog(`Create name ${taskName}`))
                setListTaskName([...listTaskName, taskName])
            }
            setTaskIndex(null)
            setTaskName("")
        }
    }

    const selectTaskName = (index) => {
        setTaskName(listTaskName[index])
        setTaskIndex(index)
    }

    const deleteTask = () => {
        if (!!taskIndex || taskIndex === 0) {
            let newTaskName = [...listTaskName];
            dispatch(insertLog(`Delete name ${newTaskName[taskIndex]}`))
            listTaskName.splice(taskIndex, 1);
            setTaskIndex(null)
            setTaskName("")
        }
    }

    return (
        <>
            <div style={{
                display: 'flex',
                justifyContent: 'center',
                alignItems: 'center',
                flexWrap: 'wrap'
            }}>
                <div>
                    <div style={{
                        textAlign: 'center',
                        fontSize: 20,
                        fontWeight: 'bold'
                    }}>Todo List</div>
                    <div style={{
                        background: 'black',
                        minHeight: 400,
                        minWidth: 300,
                        marginTop: 10,
                        borderRadius: 5,
                        color: 'white',
                        padding: 10
                    }}>
                        {_.map(listTaskName, (task, index) => {
                            return (
                                <div
                                    key={index}
                                    onClick={() => selectTaskName(index)}
                                    style={{
                                        borderBottom: '1px solid white',
                                        padding: 5,
                                        cursor: 'pointer'
                                    }}>{task}</div>
                            )
                        })}
                    </div>
                </div>
                <div style={{
                    padding: 10
                }}>
                    <div style={{
                        textAlign: 'center',
                        fontSize: 20,
                        fontWeight: 'bold',
                        marginBottom: 10
                    }}>Task Name</div>
                    <input
                        onFocus={toggleTyping}
                        onBlur={toggleTyping}
                        value={taskName}
                        onChange={(e) => setTaskName(e.target.value)}
                        style={{ borderRadius: 10, height: 30, width: 300 }} />
                    {isTyping ? <div style={{ color: 'red' }}>Typing...</div> : <></>}
                    <div style={{ textAlign: 'center', marginTop: 30 }}>
                        <button
                            onClick={() => actionTaskName()}
                            style={{
                                background: 'green',
                                borderRadius: 10,
                                width: 150,
                                height: 40,
                                color: 'white',
                                border: 'none',
                                cursor: 'pointer'
                            }}>BUTTON</button>
                    </div>
                    <div style={{ textAlign: 'center', marginTop: 30 }}>
                        <button style={{
                            background: isHoverDelete ? 'red' : 'transparent',
                            borderRadius: 10,
                            width: 150,
                            height: 40,
                            color: isHoverDelete ? 'white' : 'black',
                            border: 'none',
                            cursor: 'pointer',
                        }}
                            onMouseEnter={toggleHover}
                            onMouseLeave={toggleHover}
                            onClick={deleteTask}
                        >DELETE</button>
                    </div>
                </div>
                <div>
                    <div style={{
                        textAlign: 'center',
                        fontSize: 20,
                        fontWeight: 'bold'
                    }}>Activity List</div>
                    <div style={{
                        background: 'black',
                        minHeight: 400,
                        minWidth: 300,
                        marginTop: 10,
                        borderRadius: 5,
                        color: 'white',
                        padding: 10
                    }}>
                        {_.map(subHistory?.history, (his, index) => {
                            return (
                                <div
                                    key={index}
                                    style={{
                                        borderBottom: '1px solid white',
                                        padding: 5,
                                        cursor: 'pointer'
                                    }}
                                >{his}
                                </div>
                            )
                        })}
                    </div>
                </div>
            </div>
        </>
    )
}

export default Index
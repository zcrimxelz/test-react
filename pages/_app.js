import * as React from 'react';
import { Provider } from 'react-redux';
import Store from '../redux/store';

export default function App({ Component, ...pageProps }) {
    const store = Store()
    return (
        <>
            <Provider store={store}>
                <Component {...pageProps} />
            </Provider>
        </>
    );
}